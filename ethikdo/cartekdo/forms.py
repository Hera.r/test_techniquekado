from django import forms


class PaimentForm(forms.Form):
    card_number = forms.IntegerField(label="Numéro de la carte",
                                     help_text="C'est le numéro à 16 chiffres de la carte", required=True, min_value=10**15, max_value=10**16-1)
    debited_amount = forms.FloatField(
        label="Montant de l'achat", required=True)
