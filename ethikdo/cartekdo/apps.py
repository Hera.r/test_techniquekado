from django.apps import AppConfig


class CartekdoConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'cartekdo'
