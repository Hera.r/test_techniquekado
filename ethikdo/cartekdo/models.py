from django.db import models
from random import randrange
from django.contrib.auth.models import User
from django.core.validators import MaxValueValidator, MinValueValidator


def generate_number():
    while True:
        number = randrange(10**15, 10**16-1)
        cartes = Card.objects.filter(unique_number=number)
        if not cartes:
            return number


class Card(models.Model):
    amount = models.IntegerField(
        validators=[MinValueValidator(20), MaxValueValidator(250)])
    unique_number = models.BigIntegerField(default=generate_number)

    def valid_payment(self, debited_amount):
        return self.amount - debited_amount >= 0

    def amount_update(self, debited_amount):
        return self.amount - debited_amount


class Transaction(models.Model):
    client_card = models.ForeignKey(Card, on_delete=models.CASCADE)
    date_transaction = models.DateTimeField(auto_now_add=True)
    debited_amount = models.IntegerField()
    marchand = models.ForeignKey(User, on_delete=models.CASCADE)
