from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login, logout
from .forms import PaimentForm
from .models import Card, Transaction


@login_required(login_url='login')
def homePage(request):
    form = PaimentForm()
    context = {'form': form}
    if request.method == 'POST':
        form = PaimentForm(request.POST)

        if form.is_valid():
            try:
                card_number = form.cleaned_data['card_number']
                card = Card.objects.get(unique_number=card_number)
                debited_amount = form.cleaned_data['debited_amount']

                if card.valid_payment(debited_amount):
                    Transaction.objects.create(
                        client_card=card, debited_amount=debited_amount, marchand=request.user)
                    card.amount = card.amount_update(debited_amount)
                    card.save()
                    context["transaction"] = "Transaction validée avec succès!"
                else:
                    messages.info(
                        request, "Pas assez d'argent sur cette carte...")
                    form = Paiement()

            except Card.DoesNotExist:

                messages.info(request, 'Numero de carte Introuvable')
                form = PaimentForm()

    return render(request, 'cartekdo/home.html', context)


def loginPage(request):
    if request.user.is_authenticated:
        return redirect('home')
    else:
        if request.method == 'POST':
            username = request.POST.get('username')
            password = request.POST.get('password')

            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect('home')
            else:
                messages.info(request, 'Username OR password is incorrect')

        context = {}
        return render(request, 'cartekdo/login.html', context)


def logoutUser(request):
    logout(request)
    return redirect('login')
