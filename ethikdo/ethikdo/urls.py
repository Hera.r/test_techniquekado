from django.contrib import admin
from django.urls import include, path

urlpatterns = [
    path('cartekdo/', include('cartekdo.urls')),
    path('admin/', admin.site.urls),
]
